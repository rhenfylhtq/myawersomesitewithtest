<?php


$login = filter_var(trim($_POST['login']), FILTER_SANITIZE_STRING);
$pass = filter_var(trim($_POST['pass']), FILTER_SANITIZE_STRING);

//$pass = password_hash($pass, PASSWORD_DEFAULT);

$dsn = 'mysql:dbname=test_site;host=127.0.0.1';
$user = 'root';
$password = 'root';

try {
    $connect = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Подключение не удалось: ' . $e->getMessage();
}

$text = <<<TAG
SELECT * FROM autor WHERE login = "$login" AND pass = "$pass"
TAG;

$auth = $connect->query($text);
$auth->execute();

$user = $auth->fetch(PDO::FETCH_LAZY);

if ($user) {
    echo "Авторизация удалась";
    exit();
} else {
    echo "Неправильный логин или пароль";
}

setcookie('user', $user['name'], time() + 3600 * 24 * 30, '/');

header('location: /');
