<!DOCTYPE html>
<html lang="ru">
<?php
require_once 'Front/HTML/head.html'
?>
<body>
<?php
require_once 'Front/HTML/header.php'
?>
<div class="container">
    <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Насколько ты не лох?</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Сколько тебе осталось жить?</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Что вы думаете о смерти?</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Кто ты из "Зачарованных"?</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Кто твой лучший друг?</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Когда Мана Небесная снисзойдет на тебя?</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Тест на суицидальные наклонности</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Кем ты был 30 жизней назад?</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Какой ты пес?</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">#картинка <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Только</li>
                    <li>Настоящая</li>
                    <li>Неприкрытая</li>
                    <li>Истина</li>
                </ul>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary">Узнать</button>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'Front/HTML/footer.php'
?>
</body>
</html>
