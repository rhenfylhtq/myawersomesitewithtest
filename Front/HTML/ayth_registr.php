<!DOCTYPE html>
<?php
require_once 'head.html'
?>
<body>

<div class="container mt-4">
    <div class="row">
        <div class="col">
            <h1>Авторизация пользователя</h1>
            <form action="../../srs/auth.php" method="post">
                <input type="text" class="form-control" name="login"
                       id="login" placeholder="Введите логин (обязательное поле)"><br>
                <input type="password" class="form-control" name="pass"
                       id="pass" placeholder="Введите пароль (обязательное поле)"><br>
                <button class="btn btn-success" type="submit">Авторизация</button>

            </form>
        </div>
        <div class="col">
            <h1>Регистрация пользователя</h1>
            <form action="../../srs/Registration.php" method="post">
                <input type="text" class="form-control" name="newlogin"
                       id="newlogin" placeholder="Введите логин (обязательное поле)"><br>
                <input type="password" class="form-control" name="newPass"
                       id="newPass" placeholder="Введите пароль (обязательное поле)"><br>
                <input type="text" class="form-control" name="name"
                       id="name" placeholder="Введите Ваше имя"><br>
                <input type="text" class="form-control" name="surname"
                       id="surname" placeholder="Введите Вашу фамилию"><br>
                <input type="text" class="form-control" name="email"
                       id="email" placeholder="Введите адрес электронной почты (обязательное поле)"><br>
                <button class="btn btn-success" type="submit">Зарегистрироваться</button>

            </form>
        </div>
    </div>
</div><br>

</body>
<?php
require_once 'footer.php'
?>
</html>