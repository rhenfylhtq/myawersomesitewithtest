<!DOCTYPE html>
<html lang="en">
<?php
require_once 'head.html'
?>
<body>
<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
        <div class="col-12 col-md">
            <img class="mb-2" src="/docs/4.5/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
            <small class="d-block mb-3 text-muted">© 2020-2020</small>
        </div>
        <div class="col-6 col-md">
            <h5>Контакты</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">#</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <h5>О нас</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">DreamTeam</a></li>
            </ul>
        </div>
    </div>
</footer>
</body>
</html>